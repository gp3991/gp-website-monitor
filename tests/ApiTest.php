<?php

namespace App\Tests;

use App\Enum\SettingEnum;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiTest extends WebTestCase
{
    public const DEFAULT_COLLECTION_PAGE = 1;
    public const DEFAULT_COLLECTION_PER_PAGE = 10;

    public const BASE_API_PATH = '/api';

    public const PATH_GET_DASHBOARD = 'dashboard';

    public const PATH_GET_WEBSITES = 'websites';

    public const PATH_GET_CONNECTION = 'connections/%s';
    public const PATH_GET_WEBSITE_CONNECTIONS = 'websites/%d/connections';

    public const PATH_GET_SETTINGS = 'settings';
    public const PATH_PUT_SETTING = 'settings/%s';

    private KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    private function performApiRequest(
        string $action,
        array $data = [],
        string $rawBodyData = null,
        string $method = Request::METHOD_GET
    ): Response {
        $this->client->request(
            $method,
            sprintf('%s/%s', self::BASE_API_PATH, $action),
            $data,
            server: [
                'CONTENT_TYPE' => 'application/json',
            ],
            content: $rawBodyData
        );

        return $this->client->getResponse();
    }

    private function assertCollectionResponseMetadata(array $item, int $page, int $perPage)
    {
        $this->assertArrayHasKey('page', $item);
        $this->assertArrayHasKey('perPage', $item);
        $this->assertArrayHasKey('allResultsCount', $item);
        $this->assertArrayHasKey('pagesCount', $item);

        $this->assertEquals($page, $item['page']);
        $this->assertEquals($perPage, $item['perPage']);
    }

    private function assertCollectionResponse(array $item)
    {
        $this->assertArrayHasKey('collection', $item);
        $this->assertArrayHasKey('metadata', $item);
    }

    private function assertWebsiteCollectionItem(array $item)
    {
        $this->assertArrayHasKey('id', $item);
        $this->assertArrayHasKey('url', $item);
        $this->assertArrayHasKey('uptime', $item);
        $this->assertArrayHasKey('averageResponseTime', $item);
        $this->assertArrayHasKey('upFor24Hours', $item);
        $this->assertArrayHasKey('last15Connections', $item);
        $this->assertArrayHasKey('lastErrorDate', $item);
    }

    private function assertSetting(array $item)
    {
        $this->assertArrayHasKey('key', $item);
        $this->assertArrayHasKey('value', $item);
        $this->assertArrayHasKey('type', $item);
    }

    private function assertConnection(array $item)
    {
        $this->assertArrayHasKey('id', $item);
        $this->assertArrayHasKey('status', $item);
        $this->assertArrayHasKey('valid', $item);
        $this->assertArrayHasKey('executionTime', $item);
    }

    public function testPagination()
    {
        $page = 1;
        $perPage = 2;

        $response = $this->performApiRequest(self::PATH_GET_WEBSITES, [
            'page' => $page, 'perPage' => $perPage,
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);
        $this->assertCollectionResponse($responseData);
        $this->assertCollectionResponseMetadata($responseData['metadata'], $page, $perPage);
        $this->assertCount($perPage, $responseData['collection']);

        $firstPage = $responseData['collection'];

        ++$page;

        $response = $this->performApiRequest(self::PATH_GET_WEBSITES, [
            'page' => $page, 'perPage' => $perPage,
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);
        $this->assertCollectionResponse($responseData);
        $this->assertCollectionResponseMetadata($responseData['metadata'], $page, $perPage);
        $this->assertCount($perPage, $responseData['collection']);

        $secondPage = $responseData['collection'];

        $this->assertCount($perPage, $responseData['collection']);
        $this->assertNotEquals($firstPage, $secondPage);
    }

    public function testGetDashboard()
    {
        $response = $this->performApiRequest(self::PATH_GET_DASHBOARD);
        $this->assertEquals(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('uptime', $responseData);
    }

    public function testGetWebsites()
    {
        $page = self::DEFAULT_COLLECTION_PAGE;
        $perPage = self::DEFAULT_COLLECTION_PER_PAGE;

        $response = $this->performApiRequest(self::PATH_GET_WEBSITES, [
            'page' => $page, 'perPage' => $perPage,
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);
        $this->assertCollectionResponse($responseData);
        $this->assertCollectionResponseMetadata($responseData['metadata'], $page, $perPage);

        foreach ($responseData['collection'] as $item) {
            $this->assertWebsiteCollectionItem($item);
        }

        return $responseData['collection'][0];
    }

    /** @depends testGetWebsites */
    public function testGetWebsiteConnections($website)
    {
        $page = self::DEFAULT_COLLECTION_PAGE;
        $perPage = self::DEFAULT_COLLECTION_PER_PAGE;

        $response = $this->performApiRequest(
            sprintf(
                self::PATH_GET_WEBSITE_CONNECTIONS,
                $website['id']
            ),
            ['page' => $page, 'perPage' => $perPage]
        );
        $this->assertEquals(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);
        $this->assertCollectionResponse($responseData);
        $this->assertCollectionResponseMetadata($responseData['metadata'], $page, $perPage);

        foreach ($responseData['collection'] as $item) {
            $this->assertConnection($item);
        }

        return $responseData['collection'][0];
    }

    /** @depends testGetWebsiteConnections */
    public function testGetWebsiteConnection($connection)
    {
        $response = $this->performApiRequest(
            sprintf(
                self::PATH_GET_CONNECTION,
                $connection['id']
            )
        );
        $this->assertEquals(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);
        $this->assertConnection($responseData);
    }

    public function testGetSettings()
    {
        $response = $this->performApiRequest(self::PATH_GET_SETTINGS);
        $this->assertEquals(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);

        foreach ($responseData as $item) {
            $this->assertSetting($item);
        }
    }

    public function testUpdateSetting()
    {
        $settingKey = SettingEnum::NOTIFICATION_EMAIL()->getValue();
        $email = sprintf('%s@example.com', uniqid());

        $response = $this->performApiRequest(
            sprintf(
                self::PATH_PUT_SETTING,
                $settingKey
            ),
            [],
            json_encode(['value' => $email]),
            Request::METHOD_PUT
        );
        $this->assertEquals(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);
        $this->assertSetting($responseData);
        $this->assertEquals($responseData['value'], $email);

        // Check if setting is saved

        $response = $this->performApiRequest(self::PATH_GET_SETTINGS);
        $this->assertEquals(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);

        $key = array_search($settingKey, array_column($responseData, 'key'));
        $this->assertNotFalse($key);
        $this->assertEquals($responseData[$key]['value'], $email);
    }
}

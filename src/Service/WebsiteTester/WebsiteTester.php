<?php

namespace App\Service\WebsiteTester;

use App\Entity\Connection;
use App\Entity\Website;
use App\Enum\ConnectionErrorTypeEnum;
use App\Service\HttpClient\HttpClientInterface;
use App\Service\InternetConnectionChecker;
use App\Service\NotificationsManager;
use App\Service\SettingsManager;
use Doctrine\ORM\EntityManagerInterface;

class WebsiteTester implements WebsiteTesterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private HttpClientInterface $client,
        private SettingsManager $settings,
        private NotificationsManager $notifications,
        private InternetConnectionChecker $internetConnectionChecker
    ) {
    }

    public function performTest(Website $website): Connection
    {
        $connection = new Connection();
        $connection->setWebsite($website);
        $connection->setPerformedAt(new \DateTimeImmutable());

        if (!$this->internetConnectionChecker->checkInternetConnection()) {
            return $this->saveInternalFailConnection(
                $connection,
                Connection::STATUS_NO_INTERNET_CONNECTION,
                ConnectionErrorTypeEnum::INTERNAL()
            );
        }

        $response = $this->client->request($website->getUrl());

        if (null === $response || is_int($response)) {
            $this->saveInternalFailConnection(
                $connection,
                $response ?? Connection::STATUS_CURL_INIT_ERROR,
                ConnectionErrorTypeEnum::INTERNAL()
            );

            $this->postTest($connection);
            return $connection;
        }

        $connection->setStatus($response->getStatusCode());
        $connection->setExecutionTime($response->getExecutionTime());

        $this->entityManager->persist($connection);
        $this->entityManager->flush();

        $this->postTest($connection);

        return $connection;
    }

    /**
     * Stuff to do after performTest method.
     */
    private function postTest(Connection $connection)
    {
        // Check connection status and send notification if notifications are enabled
        if (!$connection->isValid() && $this->settings->notificationsEnabled()) {
            $this->notifications->sendWebsiteDownNotification($connection);
        }
    }

    private function saveInternalFailConnection(
        Connection $connection,
        int $status,
        ConnectionErrorTypeEnum $errorTypeEnum
    ): Connection {
        $connection->setValid(false);
        $connection->setStatus($status, false);
        $connection->setErrorType($errorTypeEnum);
        $this->entityManager->persist($connection);
        $this->entityManager->flush();
        return $connection;
    }
}

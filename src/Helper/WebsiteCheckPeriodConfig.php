<?php

namespace App\Helper;

use App\Enum\WebsiteCheckPeriodEnum;
use JetBrains\PhpStorm\ArrayShape;

class WebsiteCheckPeriodConfig
{
    private const CONFIG_SHAPE = [
        'name' => 'string',
        'cron' => 'string',
    ];

    private const CONFIGS = [
        WebsiteCheckPeriodEnum::EVERY_1_MINUTE => [
            'name' => '1 minute',
            'cron' => '* * * * *',
        ],

        WebsiteCheckPeriodEnum::EVERY_5_MINUTES => [
            'name' => '5 minutes',
            'cron' => '*/5 * * * *',
        ],

        WebsiteCheckPeriodEnum::EVERY_15_MINUTES => [
            'name' => '15 minutes',
            'cron' => '*/15 * * * *',
        ],

        WebsiteCheckPeriodEnum::EVERY_30_MINUTES => [
            'name' => '30 minutes',
            'cron' => '*/30 * * * *',
        ],

        WebsiteCheckPeriodEnum::EVERY_1_HOUR => [
            'name' => '1 hour',
            'cron' => '0 * * * *',
        ],

        WebsiteCheckPeriodEnum::EVERY_2_HOURS => [
            'name' => '2 hours',
            'cron' => '0 */2 * * *',
        ],
    ];

    #[ArrayShape(self::CONFIG_SHAPE)]
    public static function getConfig(WebsiteCheckPeriodEnum $enum): ?array
    {
        return self::CONFIGS[$enum->getValue()] ?? null;
    }
}

<?php

namespace App\Service;

use App\Dto\Transformer\DataTransformerInterface;
use App\Exception\DataTransformerException;
use App\Exception\DtoClassException;

class DtoService
{
    /** @var DataTransformerInterface[] */
    private array $transformers = [];

    /**
     * @throws DtoClassException
     */
    private function checkDtoClass(string $class): void
    {
        if (!class_exists($class)) {
            throw new DtoClassException($class);
        }
    }

    /**
     * @throws DtoClassException|DataTransformerException
     */
    public function transformObject(object $source, string $target): object
    {
        $this->checkDtoClass($target);

        $validTransformer = null;

        foreach ($this->transformers as $transformer) {
            if ($transformer->supports($source, $target)) {
                $validTransformer = $transformer;
                break;
            }
        }

        if (!$validTransformer) {
            throw new DataTransformerException(get_class($source), $target);
        }

        return $validTransformer->transform($source, $target);
    }

    /**
     * @throws DtoClassException|DataTransformerException
     */
    public function transformCollection(array $collection, string $target): array
    {
        $results = [];

        foreach ($collection as $item) {
            $results[] = $this->transformObject($item, $target);
        }

        return $results;
    }

    public function addTransformer(DataTransformerInterface $dataTransformer): void
    {
        $this->transformers[] = $dataTransformer;
    }
}

<?php

namespace App\Command;

use App\Enum\WebsiteCheckPeriodEnum;
use App\Helper\WebsiteCheckPeriodConfig;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:prepare-crontab',
    description: 'Prepare final crontab file and save it.',
)]
class PrepareCrontabCommand extends Command
{
    private string $finalCrontabPath;

    public function __construct(
        private string $baseCrontabFile,
        private string $projectDir,
        private string $checkWebsitesCommand
    ) {
        $this->finalCrontabPath = $projectDir.'/var/crontab.txt';
        parent::__construct(null);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->text(
            sprintf('Get data from: %s file', $this->baseCrontabFile)
        );

        if (!file_exists($this->baseCrontabFile)) {
            $io->error('File not found. Check BASE_CRONTAB_FILE env variable.');

            return Command::FAILURE;
        }

        $baseCrontab = file_get_contents($this->baseCrontabFile);
        $finalCrontab = $baseCrontab;

        $io->text('Add website check period entries to crontab.');

        $finalCrontab .= $this->generateWebsiteCheckPeriodCrontab();

        if (file_exists($this->finalCrontabPath)) {
            $io->text('Removing old crontab file');
            unlink($this->finalCrontabPath);
        }

        file_put_contents($this->finalCrontabPath, $finalCrontab);

        $io->success(
            sprintf('OK! New crontab file saved to %s', $this->finalCrontabPath)
        );

        return Command::SUCCESS;
    }

    private function generateWebsiteCheckPeriodCrontab(): string
    {
        $crontab = '';
        $periods = WebsiteCheckPeriodEnum::values();

        if (!count($periods)) {
            return $crontab;
        }

        $crontab .= sprintf(
            '%1$s%1$s# website check period >%1$s',
            PHP_EOL
        );

        foreach ($periods as $period) {
            $config = WebsiteCheckPeriodConfig::getConfig($period);
            $crontab .= sprintf(
                '%s /usr/local/bin/php %s/bin/console %s%s',
                $config['cron'],
                $this->projectDir,
                sprintf(
                    $this->checkWebsitesCommand,
                    $period->getValue()
                ),
                PHP_EOL
            );
        }

        $crontab .= '# website check period <'.PHP_EOL;

        return $crontab;
    }
}

<?php

namespace App\Entity;

use App\Enum\WebsiteCheckPeriodEnum;
use App\Repository\WebsiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * @ORM\Entity(repositoryClass=WebsiteRepository::class)
 */
class Website
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $url;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $locked = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $enabled = true;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     */
    private int $secondsInterval = WebsiteCheckPeriodEnum::EVERY_1_MINUTE;

    /**
     * @var Connection[]|Collection
     * @ORM\OneToMany(targetEntity=Connection::class, mappedBy="website", orphanRemoval=true)
     */
    #[Ignore]
    private array|Collection $connections;

    public function __construct()
    {
        $this->connections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getLocked(): ?string
    {
        return $this->locked;
    }

    public function setLocked(string $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getSecondsInterval(): ?WebsiteCheckPeriodEnum
    {
        return WebsiteCheckPeriodEnum::createFromSeconds($this->secondsInterval);
    }

    public function setSecondsInterval(WebsiteCheckPeriodEnum|int $periodEnum): self
    {
        $this->secondsInterval =
            $periodEnum instanceof WebsiteCheckPeriodEnum ?
            $periodEnum->getValue() : $periodEnum;

        return $this;
    }

    /**
     * @return Connection[]|Collection
     */
    public function getConnections(): array|Collection
    {
        return $this->connections;
    }

    public function addConnection(Connection $connection): self
    {
        if (!$this->connections->contains($connection)) {
            $this->connections[] = $connection;
            $connection->setWebsite($this);
        }

        return $this;
    }

    public function removeConnection(Connection $connection): self
    {
        if ($this->connections->removeElement($connection)) {
            // set the owning side to null (unless already changed)
            if ($connection->getWebsite() === $this) {
                $connection->setWebsite(null);
            }
        }

        return $this;
    }
}

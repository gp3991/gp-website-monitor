<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class AbstractApiException extends HttpException
{
    public const MESSAGE = 'An error occurred.';
    public const CODE = 500; // ?

    public function __construct(
        ?string $message = null,
        ?int $code = null
    ) {
        parent::__construct(
            $code ?? static::CODE,
            $message ?? static::MESSAGE
        );
    }

    public function toJsonResponse(?array $additionalData = null): JsonResponse
    {
        $responseData = [
            'statusCode' => $this->getStatusCode(),
            'message' => $this->getMessage(),
        ];

        $responseData = $additionalData ?
            array_merge($responseData, $additionalData) : $responseData;

        return new JsonResponse(
            $responseData,
            $this->getStatusCode()
        );
    }
}

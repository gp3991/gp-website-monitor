<?php

namespace App\Message;

class WebsiteCheckMessage
{
    public int $websiteId;

    public function __construct(int $websiteId)
    {
        $this->websiteId = $websiteId;
    }
}

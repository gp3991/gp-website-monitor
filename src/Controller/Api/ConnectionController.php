<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Entity\Connection;
use App\Enum\HttpEnum;
use App\Repository\ConnectionRepository;
use App\Service\DtoService;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Connection")
 */
class ConnectionController extends AbstractApiController
{
    public function __construct(
        private ConnectionRepository $connectionRepository,
        EntityManagerInterface $entityManager,
        DtoService $dtoService,
    ) {
        parent::__construct($entityManager, $dtoService);
    }

    /**
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      description="Connection ID",
     *      required=true
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Get connections list.",
     *     @Model(type=Connection::class)
     * )
     */
    #[Route('/api/connections/{id}', name: 'api_connection', methods: [HttpEnum::GET])]
    public function getAction(int $id): JsonResponse
    {
        $connection = $this->connectionRepository->find($id);
        if (!$connection) {
            return $this->notFoundError(
                sprintf('Connection with id %d was not found', $id)
            );
        }

        return $this->jsonResponse(
            $this->serializer->normalize($connection)
        );
    }
}

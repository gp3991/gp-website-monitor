<?php

namespace App\Exception;

class JsonRequestDecodeException extends BadRequestException
{
    public const MESSAGE = 'Error occurred while decoding json body content.';
}

#!/bin/sh

npm install

if [ "$WEBPACK_ENV" == "dev" ]
then
  npm run dev-server
else
  npm run build
fi



exit 0

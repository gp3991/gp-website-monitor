<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FakeWebsiteController extends AbstractController
{
    #[Route('/randomError', name: 'random_error')]
    public function index(ParameterBagInterface $parameterBag): Response
    {
        return new Response(status: (rand(0, 100) <= 15) ? 500 : 200);
    }
}

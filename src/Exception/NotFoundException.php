<?php

namespace App\Exception;

/**
 * Exception used to throw in API controller when some data is not found.
 */
class NotFoundException extends AbstractApiException
{
    public const MESSAGE = 'Not found.';
    public const CODE = 404;
}

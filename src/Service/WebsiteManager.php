<?php

namespace App\Service;

use App\Entity\Website;
use App\Repository\ConnectionRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class WebsiteManager
{
    public function __construct(
        public ConnectionRepository $connectionRepository
    ) {
    }

    public function getUptime(Website $website): float
    {
        try {
            $validConnections = $this->connectionRepository
                ->createQueryBuilder('c')
                ->select('count(c)')
                ->andWhere('c.website = :website')
                ->andWhere('c.valid = 1')
                ->setParameters([
                    'website' => $website,
                ])
                ->getQuery()->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException) {
            $validConnections = 0;
        }

        try {
            $invalidConnections = $this->connectionRepository
                ->createQueryBuilder('c')
                ->select('count(c)')
                ->andWhere('c.website = :website')
                ->andWhere('c.valid = 0')
                ->setParameters([
                    'website' => $website,
                ])
                ->getQuery()->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException) {
            $invalidConnections = 0;
        }

        $allConnections = $validConnections + $invalidConnections;

        if (!$allConnections) {
            return 0;
        }

        if ($invalidConnections >= $validConnections) {
            return 100;
        }

        return round($validConnections * 100 / $allConnections, 2);
    }

    public function getAverageResponseTime(Website $website): float
    {
        try {
            return $this->connectionRepository
                ->createQueryBuilder('c')
                ->select('avg(c.executionTime)')
                ->andWhere('c.website = :website')
                ->setParameters([
                    'website' => $website,
                ])
                ->getQuery()->getSingleScalarResult() ?? 0;
        } catch (NoResultException|NonUniqueResultException) {
            return 0;
        }
    }
}

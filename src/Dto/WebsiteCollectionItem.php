<?php

namespace App\Dto;

use App\Entity\Connection;

class WebsiteCollectionItem
{
    public int $id;
    public string $url;
    public float $uptime;
    public float $averageResponseTime;
    public bool $upFor24Hours;

    /** @var Connection[] */
    public array $last15Connections;

    public ?string $lastErrorDate;

    public function __construct(
        int $id,
        string $url,
        float $uptime,
        float $averageResponseTime,
        bool $upFor24Hours,
        array $last15Connections,
        ?string $lastErrorDate,
    ) {
        $this->id = $id;
        $this->url = $url;
        $this->uptime = $uptime;
        $this->averageResponseTime = $averageResponseTime;
        $this->upFor24Hours = $upFor24Hours;
        $this->last15Connections = $last15Connections;
        $this->lastErrorDate = $lastErrorDate;
    }
}

<?php

namespace App\Service\WebsiteTester;

use App\Entity\Connection;
use App\Entity\Website;

interface WebsiteTesterInterface
{
    public function performTest(Website $website): Connection;
}
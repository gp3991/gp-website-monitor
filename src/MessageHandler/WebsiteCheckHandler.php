<?php

namespace App\MessageHandler;

use App\Message\WebsiteCheckMessage;
use App\Repository\WebsiteRepository;
use App\Service\WebsiteTester\WebsiteTester;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class WebsiteCheckHandler implements MessageHandlerInterface
{
    public function __construct(
        private WebsiteRepository $websiteRepository,
        private WebsiteTester $websiteTester
    ) {
    }

    public function __invoke(WebsiteCheckMessage $message)
    {
        $website = $this->websiteRepository->find($message->websiteId);
        if (!$website) {
            return;
        }

        $this->websiteTester->performTest($website);
    }
}

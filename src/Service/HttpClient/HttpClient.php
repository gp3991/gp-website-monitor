<?php

namespace App\Service\HttpClient;

class HttpClient implements HttpClientInterface
{
    public const USERAGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36';

    public function request(string $url): null|int|HttpClientResponse
    {
        $ch = curl_init($url);

        if (false === $ch) {
            return null;
        }

        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, self::USERAGENT);
        curl_exec($ch);

        if ($errNo = curl_errno($ch)) {
            curl_close($ch);

            return $errNo;
        }

        curl_close($ch);

        return new HttpClientResponse(
            (int) curl_getinfo($ch, CURLINFO_HTTP_CODE),
            round(curl_getinfo($ch, CURLINFO_PRETRANSFER_TIME) * 1000),
            $url
        );
    }
}

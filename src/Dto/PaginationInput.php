<?php

namespace App\Dto;

class PaginationInput
{
    public const PAGE_PARAMETER_NAME = 'page';
    public const PER_PAGE_PARAMETER_NAME = 'perPage';

    public const DEFAULT_PER_PAGE = 10;

    public int $page;
    public int $perPage = self::DEFAULT_PER_PAGE;

    public function __construct(int $page, int $perPage)
    {
        $this->page = $page;
        $this->perPage = $perPage;
    }
}

<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Dto\SettingPutInput;
use App\Entity\Setting;
use App\Enum\HttpEnum;
use App\Enum\SettingEnum;
use App\Exception\ConstraintViolationListException;
use App\Exception\SettingEnumKeyNotFoundException;
use App\Exception\SettingNotFoundException;
use App\Repository\SettingRepository;
use App\Service\DtoService;
use App\Service\SettingsManager;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @OA\Tag(name="Setting")
 */
class SettingsController extends AbstractApiController
{
    public function __construct(
        private SettingRepository $settingRepository,
        private SettingsManager $settingsManager,
        private ValidatorInterface $validator,
        EntityManagerInterface $entityManager,
        DtoService $dtoService,
    ) {
        parent::__construct($entityManager, $dtoService);
    }

    /**
     * @OA\Response(
     *     response=200,
     *     description="Get settings list.",
     *     @OA\JsonContent(
     *          type="array",
     *          @OA\Items(ref=@Model(type=Setting::class))
     *     )
     * )
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    #[Route(
        '/api/settings',
        name: 'api_settings_list',
        methods: [HttpEnum::GET]
    )
    ]
    public function listAction(Request $request): JsonResponse
    {
        $settingKeys = SettingEnum::keys();
        $collection = [];

        foreach ($settingKeys as $settingKey) {
            try {
                $settingEntity = $this->settingsManager
                    ->get(SettingEnum::{$settingKey}());
                $settingNormalized = $this->serializer
                    ->normalize($settingEntity);

                $collection[] = $settingNormalized;
            } catch (SettingNotFoundException) {
                // Just ignore it...
            }
        }

        return $this->jsonResponse($collection);
    }

    /**
     * @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(ref=@Model(type=SettingPutInput::class))
     * )
     *
     * @OA\Parameter(
     *      name="key",
     *      in="path",
     *      description="Setting key",
     *      required=true,
     *      @OA\Schema(type="string")
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Get connections list.",
     *     @Model(type=Setting::class)
     * )
     *
     * @throws SettingNotFoundException
     * @throws SettingEnumKeyNotFoundException
     */
    #[Route(
        '/api/settings/{key}',
        name: 'api_settings_put',
        methods: [HttpEnum::PUT]
    )]
    public function putAction(string $key, Request $request): JsonResponse
    {
        $input = SettingPutInput::createFromRequest($request);
        $validation = $this->validator->validate($input);

        if ($validation->count()) {
            throw new ConstraintViolationListException($validation);
        }

        if (!SettingEnum::isValid($key)) {
            throw new SettingEnumKeyNotFoundException($key);
        }

        $settingEnum = SettingEnum::from($key);
        $settingEntity = $this->settingsManager->get($settingEnum);

        // Update setting entity
        $this->settingsManager->set($settingEnum, $input->value);

        $this->entityManager->refresh($settingEntity);

        return $this->json($settingEntity);
    }
}

<?php

namespace App\Dto;

use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class SettingPutInput implements RequestInputInterface
{
    /**
     * Use type `string` for OpenApi property. It's getting weird when its `mixed`...
     *
     * @OA\Property(type="string")
     */
    #[Assert\NotBlank]
    public mixed $value;

    public static function createFromRequest(Request $request): self
    {
        $dto = new self();

        if ($request->request->has('value')) {
            $dto->value = $request->get('value');
        }

        return $dto;
    }
}

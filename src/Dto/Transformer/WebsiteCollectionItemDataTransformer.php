<?php

namespace App\Dto\Transformer;

use App\Dto\WebsiteCollectionItem;
use App\Entity\Website;
use App\Repository\ConnectionRepository;
use App\Service\WebsiteManager;

class WebsiteCollectionItemDataTransformer implements DataTransformerInterface
{
    public function __construct(
      public WebsiteManager $websiteManager,
      public ConnectionRepository $connectionRepository
    ) {
    }

    public function transform(object $source, string $output): ?WebsiteCollectionItem
    {
        if (!$source instanceof Website || WebsiteCollectionItem::class !== $output) {
            return null;
        }

        $lastError = $this->connectionRepository->findLatestErrorByWebsite($source);
        $check24HoursUptimeDate = (new \DateTime())->modify('-24 hours');

        return new WebsiteCollectionItem(
            $source->getId(),
            $source->getUrl(),
            $this->websiteManager->getUptime($source),
            round($this->websiteManager->getAverageResponseTime($source)),
            !$lastError || !$lastError->getPerformedAt() > $check24HoursUptimeDate,
            $this->connectionRepository->findLatestConnectionsByWebsite($source),
            $lastError?->getPerformedAt()->format('Y-m-d H:i:s')
        );
    }

    public function supports(object $source, string $output): bool
    {
        return
            $source instanceof Website &&
            WebsiteCollectionItem::class === $output;
    }
}

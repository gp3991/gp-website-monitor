# For recruiters

#### Hi :)

The application is used to monitor urls stored in the database. Monitoring is based on sending HTTP requests using
[Symfony HTTP client](https://symfony.com/doc/current/http_client.html). If the timeout occurs, or response code is
incorrect, a notification is sent to the e-mail address you set. Code and response time are saved for each connection.
These data will be available for inspection in frontend that will be developed in the future. At the moment, the
application has only very simple home page view and API.

Home (examples only): https://uptime.gp3991.dev  
API docs: https://uptime.gp3991.dev/api/doc

Check out the comments below and the entire repository to see if it is worth contacting me at all.

### PHP

Project running on PHP 8.0.6 with Symfony 5.3. The app is very simple, see the most interesting places below.

Simple DTO data transformers system for API responses.

- [App\Dto\Transformer\DataTransformerInterface](src/Dto/Transformer/DataTransformerInterface.php)
- [App\DependencyInjection\Compiler\DataTransformerPass](src/DependencyInjection/Compiler/DataTransformerPass.php)
- [App\Service\DtoService](src/Service/DtoService.php)
- Example
  transformer: [App\Dto\Transformer\WebsiteCollectionItemDataTransformer](src/Dto/Transformer/WebsiteCollectionItemDataTransformer.php)

I used `symfony/messenger` with RabbitMQ to queue single url calls and send notifications.

- [messenger.yaml](config/packages/messenger.yaml)
- [App\Message\WebsiteCheckMessage](src/Message/WebsiteCheckMessage.php)
- [App\MessageHandler\WebsiteCheckHandler](src/MessageHandler/WebsiteCheckHandler.php)

Symfony command as crontab generator

- [App\Command\PrepareCrontabCommand](src/Command/PrepareCrontabCommand.php)

### Docker | CI/CD

Custom Docker stack

- php-fpm
- nginx
- mysql
- rabbitmq
- webpack

See [docker-compose.yml](docker-compose.yml) or [docker](docker) for more details.

There is simple CI/CD that includes build, test and deploy stage. Look at [.gitlab-ci.yml](.gitlab-ci.yml) for more
info. Current version requires custom GitLab runner
config ([gitlab_runners_example.toml](docker/gitlab_runners_example.toml)). It's kind of an experiment, so use deploy
stage in your server at your own risk.

### PHPUnit

I wrote some tests which you will find in [tests](tests). The application was supposed to serve only me and only for a
while, so the tests are only proof that I can write some simple things.

### Frontend

Rig with a webpack, postcss, babel, Vue.js and a separate docker container to run webpack dev server or to build an
application. There is not much code, it can be said that frontend does not exist at the moment. What I have you'll find
in [assets](assets).

# Usage

Run project dev env with:

```shell
docker-compose up
```

By default, it's using [docker-compose.override.yml](docker-compose.override.yml) merged
into [docker-compose.yml](docker-compose.yml).

### Migrations

```shell
bin/docker-console doctrine:migrations:migrate 
```

### Fixtures

```shell
bin/docker-console doctrine:fixtures:load
```

### cs-fixer

Run `composer install` from fpm container in `tools/php-cs-fixer`.

Next, you can use:

```shell
bin/php-cs-fixer fix
```
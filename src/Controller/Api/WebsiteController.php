<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Dto\CollectionMetadata;
use App\Dto\WebsiteCollectionItem;
use App\Entity\Connection;
use App\Entity\Website;
use App\Enum\HttpEnum;
use App\Repository\ConnectionRepository;
use App\Repository\WebsiteRepository;
use App\Service\DtoService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Website")
 */
class WebsiteController extends AbstractApiController
{
    public function __construct(
        private WebsiteRepository $websiteRepository,
        private ConnectionRepository $connectionRepository,
        EntityManagerInterface $entityManager,
        DtoService $dtoService,
    ) {
        parent::__construct($entityManager, $dtoService);
    }

    /**
     * @OA\Parameter(ref="#/components/parameters/pagination--page")
     * @OA\Parameter(ref="#/components/parameters/pagination--perPage")
     *
     * @OA\Response(
     *     response=200,
     *     description="Get websites list.",
     *     @OA\JsonContent(
     *          @OA\Property(
     *              property="collection",
     *              type="array",
     *              @OA\Items(ref=@Model(type=WebsiteCollectionItem::class))
     *          ),
     *          @OA\Property(
     *              property="metadata",
     *              ref=@Model(type=CollectionMetadata::class)
     *          )
     *     )
     * )
     */
    #[Route('/api/websites', name: 'api_websites_list', methods: [HttpEnum::GET])]
    public function listAction(Request $request): JsonResponse
    {
        $paginationInput = $this->getPaginationRequestData($request);

        $paginator = $this->getCollection(
            Website::class,
            $paginationInput->page,
            $paginationInput->perPage,
            itemDto: WebsiteCollectionItem::class,
        );

        return $this->serializeCollection($paginator);
    }

    /**
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      description="Website ID",
     *      required=true,
     *      @OA\Schema(type="integer", minimum="1")
     * )
     *
     * @OA\Parameter(ref="#/components/parameters/pagination--page")
     * @OA\Parameter(ref="#/components/parameters/pagination--perPage")
     *
     * @OA\Response(
     *     response=200,
     *     description="Get connections list.",
     *     @OA\JsonContent(
     *          @OA\Property(
     *              property="collection",
     *              type="array",
     *              @OA\Items(ref=@Model(type=Connection::class))
     *          ),
     *          @OA\Property(
     *              property="metadata",
     *              ref=@Model(type=CollectionMetadata::class)
     *          )
     *     )
     * )
     */
    #[Route('/api/websites/{id}/connections', name: 'api_website_connections', methods: [HttpEnum::GET])]
    public function connectionsAction(int $id, Request $request): JsonResponse
    {
        if (!$website = $this->websiteRepository->find($id)) {
            return $this->notFoundError(
                sprintf('Website with id %d was not found', $id)
            );
        }

        $paginationInput = $this->getPaginationRequestData($request);

        $paginator = $this->getCollection(
            Connection::class,
            $paginationInput->page,
            $paginationInput->perPage,
            function (QueryBuilder $qb, string $rootAlias) use ($website) {
                $qb
                    ->andWhere("$rootAlias.website = :website")
                    ->setParameter('website', $website);
            }
        );

        return $this->serializeCollection($paginator);
    }
}

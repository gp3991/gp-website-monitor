<?php

namespace App\Tests;

use App\Enum\SettingEnum;
use App\Exception\SettingNotFoundException;
use App\Service\SettingsManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SettingsManagerTest extends KernelTestCase
{
    private SettingsManager $settingsManager;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->settingsManager = self::getContainer()
            ->get(SettingsManager::class);
    }

    public function testSettingUpdate()
    {
        // BOOL
        $this->settingsManager->set(SettingEnum::NOTIFICATIONS_ENABLED(), true);
        $this->assertTrue($this->settingsManager->get(SettingEnum::NOTIFICATIONS_ENABLED())?->getValue());
        $this->assertTrue($this->settingsManager->notificationsEnabled());

        $this->settingsManager->set(SettingEnum::NOTIFICATIONS_ENABLED(), false);
        $this->assertNotTrue($this->settingsManager->get(SettingEnum::NOTIFICATIONS_ENABLED())?->getValue());
        $this->assertNotTrue($this->settingsManager->notificationsEnabled());

        // STRING
        $email = sprintf('%s@example.com', uniqid());
        $this->settingsManager->set(SettingEnum::NOTIFICATION_EMAIL(), $email);
        $this->assertEquals(
            $email,
            $this->settingsManager->get(SettingEnum::NOTIFICATION_EMAIL())?->getValue()
        );
    }

    public function testExceptionIsRaisedWhenSettingObjectIsNotFoundForGetMethod()
    {
        $settingEnumMock = $this->createMock(SettingEnum::class);
        $settingEnumMock
            ->expects($this->any())
            ->method('getValue')
            ->willReturn(uniqid());

        $this->expectException(SettingNotFoundException::class);
        $this->settingsManager->get($settingEnumMock);
    }

    public function testExceptionIsRaisedWhenSettingObjectIsNotFoundForSetMethod()
    {
        $settingEnumMock = $this->createMock(SettingEnum::class);
        $settingEnumMock
            ->expects($this->any())
            ->method('getValue')
            ->willReturn(uniqid());

        $this->expectException(SettingNotFoundException::class);
        $this->settingsManager->set($settingEnumMock, uniqid());
    }
}

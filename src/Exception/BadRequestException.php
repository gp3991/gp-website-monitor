<?php

namespace App\Exception;

/**
 * Exception to use in API when some request error occurs.
 */
class BadRequestException extends AbstractApiException
{
    public const MESSAGE = 'Bad request.';
    public const CODE = 400;
}

<?php

namespace App\Exception;

class DtoClassException extends \Exception
{
    public const MESSAGE_NOT_FOUND = 'The specified class `%s` does not exist or is incorrectly configured.';

    public function __construct(string $class, $message = self::MESSAGE_NOT_FOUND)
    {
        parent::__construct(
            sprintf($message, $class)
        );
    }
}

<?php

namespace App\Dto;

class Dashboard
{
    public function __construct(
        public float $uptime
    ) {
    }
}

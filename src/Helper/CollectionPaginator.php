<?php

namespace App\Helper;

use App\Dto\CollectionMetadata;
use Doctrine\Common\Collections\Collection;
use JetBrains\PhpStorm\ArrayShape;

class CollectionPaginator
{
    public const METADATA_CONTEXT_SHAPE = [
        'page' => 'int',
        'perPage' => 'int',
        'allResultsCount' => 'float|int',
        'pagesCount' => 'float|int',
    ];

    private array|Collection $collection;

    private int $page;
    private int $perPage;

    private int $allResultsCount;
    private int $pagesCount;

    private CollectionMetadata $metadata;

    public function __construct(Collection|array $collection, int $page, int $perPage, int $allResultsCount)
    {
        $this->collection = $collection;
        $this->page = $page;
        $this->perPage = $perPage;
        $this->allResultsCount = $allResultsCount;
        $this->pagesCount = ceil($allResultsCount / $perPage);

        $this->metadata = new CollectionMetadata(
            $this->page,
            $this->perPage,
            $this->allResultsCount,
            $this->pagesCount
        );
    }

    #[ArrayShape(self::METADATA_CONTEXT_SHAPE)]
    public function getMetadata(): CollectionMetadata
    {
        return $this->metadata;
    }

    public function getCollection(): Collection|array
    {
        return $this->collection;
    }
}

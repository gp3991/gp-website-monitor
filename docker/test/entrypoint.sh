#!/bin/sh

# wait for main containers
wait-for -t 300 mysql:3306 -- echo 'mysql ok' && \
wait-for -t 300 rabbitmq:15672 -- echo 'rabbitmq ok' && \
wait-for -t 300 php-fpm:9000 -- echo 'php-fpm ok' && \
wait-for -t 300 webserver:80 -- echo 'webserver ok' && \
exit 0

exit 1

<?php

namespace App\EventListener;

use App\Exception\JsonRequestDecodeException;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class JsonRequestListener
{
    /**
     * @throws JsonRequestDecodeException
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        if ('json' !== $request->getContentType() || !strlen($request->getContent())) {
            return;
        }

        $data = json_decode($request->getContent(), true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new JsonRequestDecodeException();
        }

        $request->request->replace($data);
    }
}

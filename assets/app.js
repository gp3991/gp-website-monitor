import Vue from 'vue';
import VueResource from 'vue-resource';
import Base from './components/Base'
import vuetify from './plugins/vuetify';

import './styles/app.scss'

Vue.use(VueResource);

new Vue({
    vuetify,
    render: h => h(Base)
}).$mount('#app');
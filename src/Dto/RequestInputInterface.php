<?php

namespace App\Dto;

use Symfony\Component\HttpFoundation\Request;

interface RequestInputInterface
{
    public static function createFromRequest(Request $request): self;
}

<?php

namespace App\DependencyInjection\Compiler;

use App\Service\DtoService;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class DataTransformerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(DtoService::class)) {
            return;
        }

        $definition = $container->findDefinition(DtoService::class);
        $taggedServices = $container->findTaggedServiceIds('app.data_transformer');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addTransformer', [new Reference($id)]);
        }
    }
}

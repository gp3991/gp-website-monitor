<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ConstraintViolationListException extends AbstractApiException
{
    public const MESSAGE = 'Validation error occurred.';
    public const CODE = 400;

    private array $violations;

    public function __construct(ConstraintViolationListInterface $violationList)
    {
        /** @var ConstraintViolationInterface $violation */
        foreach ($violationList as $violation) {
            $this->violations[$violation->getPropertyPath()] = $violation->getMessage();
        }

        parent::__construct();
    }

    public function toJsonResponse(?array $additionalData = null): JsonResponse
    {
        $violationsData = ['violations' => $this->violations];

        return parent::toJsonResponse(
            $additionalData ?
                array_merge($violationsData, $additionalData) : $violationsData
        );
    }
}

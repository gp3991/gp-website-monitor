<?php

namespace App\Dto\Transformer;

interface DataTransformerInterface
{
    public function transform(object $source, string $output): ?object;
    public function supports(object $source, string $output): bool;
}

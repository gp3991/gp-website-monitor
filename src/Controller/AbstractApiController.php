<?php

namespace App\Controller;

use App\Dto\PaginationInput;
use App\Exception\DataTransformerException;
use App\Exception\DtoClassException;
use App\Helper\CollectionPaginator;
use App\Service\DtoService;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractApiController extends AbstractController
{
    protected Serializer $serializer;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected DtoService $dtoService,
    ) {
        $this->serializer = $this->createSerializer();
    }

    protected function getPaginationRequestData(Request $request): PaginationInput
    {
        return new PaginationInput(
            (int) $request->get(
                PaginationInput::PAGE_PARAMETER_NAME,
                1
            ),
            (int) $request->get(
                PaginationInput::PER_PAGE_PARAMETER_NAME,
                PaginationInput::DEFAULT_PER_PAGE
            )
        );
    }

    protected function serializeCollection(CollectionPaginator $paginator): JsonResponse
    {
        $normalized = [];

        foreach ($paginator->getCollection() as $item) {
            $normalized[] = $this->serializer->normalize($item);
        }

        return $this->jsonResponse([
            'collection' => $normalized,
            'metadata' => $paginator->getMetadata()->toArray(),
        ]);
    }

    protected function getCollection(
        string $class,
        int $page = 1,
        int $perPage = 10,
        ?callable $qbExtension = null,
        ?string $itemDto = null
    ): ?CollectionPaginator {
        if (!class_exists($class)) {
            return null;
        }

        $repository = $this->entityManager->getRepository($class);
        if (!$repository instanceof EntityRepository) {
            return null;
        }

        $baseQb = $repository->createQueryBuilder('o');
        $paginationQb = $repository->createQueryBuilder('o');

        if ($qbExtension) {
            $qbExtension($baseQb, $baseQb->getRootAliases()[0]);
            $qbExtension($paginationQb, $baseQb->getRootAliases()[0]);
        }

        $paginationQb->select('count(o)');

        $results = $baseQb
            ->setMaxResults($perPage)
            ->setFirstResult(($page - 1) * $perPage)
            ->getQuery()->getResult();

        // Convert results to target item DTO class
        if ($results && $itemDto) {
            try {
                $results = $this->dtoService
                    ->transformCollection($results, $itemDto);
            } catch (DtoClassException | DataTransformerException) {
                $results = [];
            }
        }

        try {
            return new CollectionPaginator(
                $results,
                $page,
                $perPage,
                $paginationQb->getQuery()->getSingleScalarResult()
            );
        } catch (\Exception) {
            return null;
        }
    }

    /**
     * Create and return Serializer object for doctrine entities in API.
     */
    protected function createSerializer(): Serializer
    {
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => (fn ($object, $format, $context) => $object->getId()),
        ];

        $classMetadataFactory = new ClassMetadataFactory(
            new AnnotationLoader(new AnnotationReader())
        );

        $normalizer = new ObjectNormalizer(
            $classMetadataFactory,
            null,
            null,
            null,
            null,
            null,
            $defaultContext
        );

        return new Serializer([$normalizer]);
    }

    protected function notFoundError(string $message): JsonResponse
    {
        return $this->errorResponse('not-found', $message, 404);
    }

    protected function errorResponse(string $errorCode, string $message, $code = 404): JsonResponse
    {
        return $this->jsonResponse(
            [
                'errorCode' => $errorCode,
                'message' => $message,
            ],
            $code
        );
    }

    protected function jsonResponse(array $data, int $status = 200): JsonResponse
    {
        return $this->json($data, $status);
    }
}

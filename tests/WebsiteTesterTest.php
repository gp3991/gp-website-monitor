<?php

namespace App\Tests;

use App\Entity\Connection;
use App\Entity\Website;
use App\Enum\SettingEnum;
use App\Repository\WebsiteRepository;
use App\Service\SettingsManager;
use App\Service\WebsiteTester\WebsiteTester;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class WebsiteTesterTest extends KernelTestCase
{
    public const ALWAYS_ONLINE_URL = 'https://google.com';
    public const ALWAYS_OFFLINE_URL = 'https://atluzr-jp2137.akwomerk';

    private ?EntityManagerInterface $entityManager;
    private WebsiteRepository $websiteRepository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $container = $kernel->getContainer();

        $this->entityManager = $container
            ->get('doctrine')
            ->getManager();

        $this->websiteRepository = $this->entityManager
            ->getRepository(Website::class);

        // Set notifications enabled for assertQueuedEmailCount assert
        self::getContainer()
            ->get(SettingsManager::class)
            ->set(SettingEnum::NOTIFICATIONS_ENABLED(), true);

        // Set fake notifications email for assertQueuedEmailCount assert
        self::getContainer()
            ->get(SettingsManager::class)
            ->set(SettingEnum::NOTIFICATION_EMAIL(), 'fakemail@example.com');
    }

    public function testValidUrl(): void
    {
        $websiteTester = self::getContainer()->get(WebsiteTester::class);

        $website = $this->websiteRepository->findAll()[0];
        $website->setUrl(self::ALWAYS_ONLINE_URL);
        $this->entityManager->flush();

        $connection = $websiteTester->performTest($website);

        $this->assertInstanceOf(Connection::class, $connection);
        $this->assertGreaterThan(0, $connection->getExecutionTime());
    }

    public function testInvalidUrl(): void
    {
        $websiteTester = self::getContainer()->get(WebsiteTester::class);

        $website = $this->websiteRepository->findAll()[0];
        $website->setUrl(self::ALWAYS_OFFLINE_URL);
        $this->entityManager->flush();

        $connection = $websiteTester->performTest($website);

        $this->assertInstanceOf(Connection::class, $connection);
        $this->assertEquals(false, $connection->isValid());
        $this->assertQueuedEmailCount(1);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}

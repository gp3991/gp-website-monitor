<?php

namespace App\Dto;

use JetBrains\PhpStorm\ArrayShape;

class CollectionMetadata
{
    public const ARRAY_SHAPE = [
        'page' => 'int',
        'perPage' => 'int',
        'allResultsCount' => 'int',
        'pagesCount' => 'int',
    ];

    public int $page;
    public int $perPage;
    public int $allResultsCount;
    public int $pagesCount;

    public function __construct(int $page, int $perPage, int $allResultsCount, int $pagesCount)
    {
        $this->page = $page;
        $this->perPage = $perPage;
        $this->allResultsCount = $allResultsCount;
        $this->pagesCount = $pagesCount;
    }

    #[ArrayShape(self::ARRAY_SHAPE)]
    public function toArray(): array
    {
        return [
            'page' => $this->page,
            'perPage' => $this->perPage,
            'allResultsCount' => $this->allResultsCount,
            'pagesCount' => $this->pagesCount,
        ];
    }
}

<?php

namespace App\Service;

use App\Entity\Connection;
use App\Enum\SettingEnum;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class NotificationsManager
{
    private string $emailFrom;
    private string $notificationEmail;

    public function __construct(
        private SettingsManager $settings,
        private MailerInterface $mailer,
        ParameterBagInterface $parameterBag
    ) {
        $this->emailFrom = $parameterBag->get('mailer_default_from');
        $this->notificationEmail = $this->settings
            ->get(SettingEnum::NOTIFICATION_EMAIL())
            ->getValue();
    }

    private function isItSafeToSendANotification(): bool
    {
        if (!$this->settings->notificationsEnabled()) {
            return false;
        }

        if (!filter_var($this->notificationEmail, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }

    private function prepareNotification(string $subject, string $html, string $text = ''): Email
    {
        return (new Email())
            ->from($this->emailFrom)
            ->to($this->notificationEmail)
            ->subject($subject)
            ->text($text)
            ->html($html);
    }

    public function sendWebsiteDownNotification(Connection $connection)
    {
        if (!$this->isItSafeToSendANotification()) {
            return;
        }

        $email = $this->prepareNotification(
            sprintf('%s is down!', $connection->getWebsite()->getUrl()),
            sprintf(
                '%s is down! Resonse code: %s',
                $connection->getWebsite()->getUrl(),
                $connection->getStatus()
            ),
            sprintf(
                '<p><a href="%1$s">%1$s</a> request failed!</p><p>Resonse code: %2$s</p>',
                $connection->getWebsite()->getUrl(),
                $connection->getStatus()
            )
        );

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface) {
            // TODO LOG NOTIFICATION ERROR
        }
    }

    public function sendNoInternetConnectionNotification()
    {
        if (!$this->isItSafeToSendANotification()) {
            return;
        }

        $notificationContent = sprintf(
            'Could not connect to any address: %s. Probably no internet connection.',
            implode(', ', InternetConnectionChecker::HOSTS_TO_CHECK)
        );

        $email = $this->prepareNotification(
            'Internet connection check error!',
            sprintf('<p>%s</p>', $notificationContent),
            $notificationContent
        );

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface) {
            // TODO LOG NOTIFICATION ERROR
        }
    }
}

<?php

namespace App\Command;

use App\Message\WebsiteCheckMessage;
use App\Repository\WebsiteRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(
    name: 'app:check-websites',
    description: 'Add a short description for your command',
)]
class CheckWebsitesCommand extends Command
{
    public const ARGUMENT_SECONDS_INTERVAL = 'seconds_interval';
    
    public function __construct(
        private WebsiteRepository $websiteRepository,
        private MessageBusInterface $bus,
    ) {
        parent::__construct(null);
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                self::ARGUMENT_SECONDS_INTERVAL,
                InputArgument::REQUIRED,
                'Argument description'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $secondsInterval = (int) $input->getArgument(self::ARGUMENT_SECONDS_INTERVAL);

        $websites = $this->websiteRepository->findBy(['secondsInterval' => $secondsInterval]);

        foreach ($websites as $website) {
            $this->bus->dispatch(
                new WebsiteCheckMessage($website->getId())
            );
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}

<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static WebsiteCheckPeriodEnum EVERY_1_MINUTE()
 * @method static WebsiteCheckPeriodEnum EVERY_5_MINUTES()
 * @method static WebsiteCheckPeriodEnum EVERY_15_MINUTES()
 * @method static WebsiteCheckPeriodEnum EVERY_30_MINUTES()
 * @method static WebsiteCheckPeriodEnum EVERY_1_HOUR()
 * @method static WebsiteCheckPeriodEnum EVERY_2_HOURS()
 */
class WebsiteCheckPeriodEnum extends Enum
{
    public const EVERY_1_MINUTE = 60;
    public const EVERY_5_MINUTES = 300;
    public const EVERY_15_MINUTES = 900;
    public const EVERY_30_MINUTES = 1800;
    public const EVERY_1_HOUR = 3600;
    public const EVERY_2_HOURS = 7200;

    public static function createFromSeconds(?int $seconds): ?self
    {
        $search = self::search($seconds);

        return $search ? self::{$search}() : null;
    }
}

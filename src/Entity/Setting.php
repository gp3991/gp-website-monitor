<?php

namespace App\Entity;

use App\Repository\SettingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SettingRepository::class)
 */
class Setting
{
    public const TYPE_BOOLEAN = 'boolean';
    public const TYPE_STRING = 'string';
    public const TYPE_ARRAY = 'array';

    public const DEFAULT_TYPE = self::TYPE_STRING;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=50, name="id")
     */
    private string $key;

    /**
     * @ORM\Column(type="text")
     */
    private string $value = '';

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $type = self::TYPE_STRING;

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getValue(bool $raw = false): null|string|bool|array
    {
        if ($raw) {
            return $this->value;
        }

        return match ($this->type) {
            self::TYPE_ARRAY => json_decode($this->value, true),
            self::TYPE_BOOLEAN => (bool) $this->value,
            default => $this->value
        };
    }

    public function setValue(string|bool|array $value, bool $strict = false): self
    {
        if ($strict) {
            $this->value = $value;

            return $this;
        }

        $this->value =
            match (gettype($value)) {
                self::TYPE_STRING => $value,
                self::TYPE_ARRAY => json_encode($value),
                self::TYPE_BOOLEAN => $value ? '1' : '0',
                default => (string) $value
            };

        return $this;
    }

    public function getType(): string|bool|array|null
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}

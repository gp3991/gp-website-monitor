#!/bin/sh

cd /app

# Wait for mysql
echo 'Waiting for mysql'
wait-for -t 300 mysql:3306 -- echo 'mysql connection established'

# Fix permissions TODO USE ANOTHER METHOD...
chmod -R 777 .

# Check $DEV env var (from docker-compose file) and run appropriate script
# if vendor directory does not exist
if [ $DEV ] && [ ! -d "vendor" ]
then
  composer install --no-interaction
  php bin/console cache:clear
  php bin/console doctrine:migrations:migrate --no-interaction
elif [ ! -d "vendor" ]
then
  composer install --no-dev --no-progress --no-interaction --optimize-autoloader
  composer dump-env prod
  APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
  php bin/console doctrine:migrations:migrate --no-interaction
fi

# Add docker host to /etc/hosts
/sbin/ip -4 route list match 0/0 | awk '{print $3 " host.docker.internal"}' >> /etc/hosts

# Wait for rabbitmq to start
echo 'Waiting for rabbitmq'
wait-for -t 300 rabbitmq:15672 -- echo 'rabbitmq ok'

# Initialize crontab
php /app/bin/console app:prepare-crontab
crontab /app/var/crontab.txt
service cron start

# Run FPM and start consumers
if [ $CONSUMER_VERBOSE ]
then
  php-fpm -F -R & php bin/console messenger:consume async -vv
else
  php-fpm -F -R & php bin/console messenger:consume async
fi

<?php

namespace App\Service\HttpClient;

class HttpClientResponse implements HttpClientResponseInterface
{
    public function __construct(
        private int $statusCode,
        private int $executionTime,
        private string $url
    ) {
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getExecutionTime(): int
    {
        return $this->executionTime;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}

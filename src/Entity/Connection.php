<?php

namespace App\Entity;

use App\Enum\ConnectionErrorTypeEnum;
use App\Repository\ConnectionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * @ORM\Entity(repositoryClass=ConnectionRepository::class)
 */
class Connection
{
    public const STATUS_CODE_TYPE_INFO = 1;
    public const STATUS_CODE_TYPE_SUCCESS = 2;
    public const STATUS_CODE_TYPE_REDIRECT = 3;
    public const STATUS_CODE_TYPE_CLIENT_ERROR = 4;
    public const STATUS_CODE_TYPE_SERVER_ERROR = 5;

    public const STATUS_NO_INTERNET_CONNECTION = 0;
    public const STATUS_CURL_INIT_ERROR = 0;

    public const VALID_STATUS_CODES = [
        self::STATUS_CODE_TYPE_INFO,
        self::STATUS_CODE_TYPE_SUCCESS,
        self::STATUS_CODE_TYPE_REDIRECT,
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer")
     */
    private int $status;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $valid;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $errorType;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Ignore]
    private \DateTimeImmutable $performedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Website::class, inversedBy="connections", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    #[Ignore]
    private Website $website;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $executionTime;

    private function validCheck(): bool
    {
        if (!$this->status) {
            return false;
        }

        $codeType = (int) substr($this->status, 0, 1);

        return in_array($codeType, self::VALID_STATUS_CODES);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status, $validCheck = true): self
    {
        $this->status = $status;

        if ($validCheck) {
            $this->setValid($this->validCheck());
        }

        return $this;
    }

    public function isValid(): bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): void
    {
        $this->valid = $valid;
    }

    public function getErrorType(): ?ConnectionErrorTypeEnum
    {
        return ConnectionErrorTypeEnum::isValid($this->errorType) ?
            ConnectionErrorTypeEnum::from($this->errorType) : null;
    }

    public function setErrorType(ConnectionErrorTypeEnum $enum): self
    {
        $this->errorType = $enum->getValue();

        return $this;
    }

    public function getPerformedAt(): ?\DateTimeImmutable
    {
        return $this->performedAt;
    }

    public function setPerformedAt(\DateTimeImmutable $performedAt): self
    {
        $this->performedAt = $performedAt;

        return $this;
    }

    public function getWebsite(): ?Website
    {
        return $this->website;
    }

    public function setWebsite(?Website $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getExecutionTime(): ?int
    {
        return $this->executionTime;
    }

    public function setExecutionTime(?int $executionTime): self
    {
        $this->executionTime = $executionTime;

        return $this;
    }
}

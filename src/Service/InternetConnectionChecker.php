<?php

namespace App\Service;

class InternetConnectionChecker
{
    public const HOSTS_TO_CHECK = ['1.1.1.1', '1.0.0.1', '8.8.8.8', '8.8.4.4'];

    public function __construct(
        private NotificationsManager $notifications
    ) {
    }

    /**
     * Check internet connection to most popular DNS servers.
     */
    public function checkInternetConnection(): bool
    {
        foreach (self::HOSTS_TO_CHECK as $host) {
            if ($connected = @fsockopen($host, 443)) {
                fclose($connected);

                return true;
            }
        }

        $this->notifications->sendNoInternetConnectionNotification();
        return false;
    }
}

<?php

namespace App\Enum;

class HttpEnum
{
    public const GET = 'GET';
    public const POST = 'POST';
    public const PUT = 'PUT';
}

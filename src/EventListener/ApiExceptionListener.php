<?php

namespace App\EventListener;

use App\Exception\AbstractApiException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Listener used to handle HttpException throwable.
 * Pick exception when thrown and transform it to JsonResponse for better API errors handling.
 */
class ApiExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        // Handle only Symfony\Component\HttpKernel\Exception\HttpException
        if (!$exception instanceof HttpException) {
            return;
        }

        // If not App\Exception\AbstractApiException then use standard transform
        if (!$exception instanceof AbstractApiException) {
            $this->setStandardResponse($event, $exception);

            return;
        }

        $event->setResponse($exception->toJsonResponse());
    }

    private function setStandardResponse(ExceptionEvent $event, HttpException $exception)
    {
        $response = new JsonResponse(
            [
                'statusCode' => $exception->getStatusCode(),
                'message' => $exception->getMessage(),
            ],
            $exception->getStatusCode()
        );

        $event->setResponse($response);
    }
}

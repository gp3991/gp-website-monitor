<?php

namespace App\Tests;

use App\Service\InternetConnectionChecker;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class InternetConnectionCheckerTest extends KernelTestCase
{
    public const HOSTS_TO_CHECK = ['1.1.1.1', '1.0.0.1', '8.8.8.8', '8.8.4.4'];

    private InternetConnectionChecker $internetConnectionChecker;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->internetConnectionChecker = self::getContainer()
            ->get(InternetConnectionChecker::class);
    }

    public function testInternetConnectionChecker()
    {
        $validResult = $this->checkInternetConnection();
        $serviceResult = $this->internetConnectionChecker->checkInternetConnection();

        $this->assertEquals($validResult, $serviceResult);
    }

    /**
     * Basic (and hopefully working) method to test internet connection.
     * Used to compare with InternetConnectionChecker::checkInternetConnection method.
     */
    private function checkInternetConnection(): bool
    {
        foreach (self::HOSTS_TO_CHECK as $host) {
            if ($connected = @fsockopen($host, 443)) {
                fclose($connected);

                return true;
            }
        }

        return false;
    }
}

<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Dto\Dashboard;
use App\Enum\HttpEnum;
use App\Helper\DashboardFactory;
use App\Service\DtoService;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Dashboard")
 */
class DashboardController extends AbstractApiController
{
    public function __construct(
        private DashboardFactory $dashboardFactory,
        EntityManagerInterface $entityManager,
        DtoService $dtoService,
    ) {
        parent::__construct($entityManager, $dtoService);
    }

    /**
     * @OA\Response(
     *     response=200,
     *     description="Get dashboard data.",
     *     @Model(type=Dashboard::class)
     * )
     */
    #[Route('/api/dashboard', name: 'api_dashboard', methods: [HttpEnum::GET])]
    public function getAction(): JsonResponse
    {
        return $this->json(
            $this->dashboardFactory->createDashboard()
        );
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Connection;
use App\Entity\Website;
use App\Enum\WebsiteCheckPeriodEnum;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class WebsiteFixtures extends Fixture
{
    public const WEBSITES = [
        [
            'url' => 'https://google.com',
            'locked' => true,
            'duration' => WebsiteCheckPeriodEnum::EVERY_1_MINUTE,
        ],
        [
            'url' => 'https://youtube.com',
            'locked' => true,
            'duration' => WebsiteCheckPeriodEnum::EVERY_5_MINUTES,
        ],
        [
            'url' => 'https://wikipedia.org',
            'duration' => WebsiteCheckPeriodEnum::EVERY_2_HOURS,
        ],
        [
            'url' => 'https://facebook.com',
            'duration' => WebsiteCheckPeriodEnum::EVERY_15_MINUTES,
        ],
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::WEBSITES as $key => $website) {
            $entity = new Website();
            $entity->setUrl($website['url']);
            $entity->setLocked($website['locked'] ?? false);
            $entity->setSecondsInterval($website['duration']);
            $this->generateConnections($manager, $entity);
            $manager->persist($entity);

            $this->addReference(Website::class.'_'.$key, $entity);
        }

        $manager->flush();
    }

    private function generateConnections(ObjectManager $manager, Website $website)
    {
        $performedAt = new DateTime();

        for ($i = 0; $i < rand(30, 100); ++$i) {
            $newConnection = new Connection();
            $newConnection->setWebsite($website);
            $newConnection->setValid(true);
            $newConnection->setExecutionTime(rand(10, 100));
            $newConnection->setStatus(200);
            $newConnection->setPerformedAt(\DateTimeImmutable::createFromMutable($performedAt));

            $website->addConnection($newConnection);

            $performedAt->modify(sprintf(
                '-%d seconds',
                $website->getSecondsInterval()->getValue()
            ));

            $manager->persist($newConnection);
        }
    }
}

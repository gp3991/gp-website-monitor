<?php

namespace App\Service\HttpClient;

interface HttpClientInterface
{
    public function request(string $url): null|int|HttpClientResponse;
}

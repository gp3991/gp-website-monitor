#!/bin/bash

verify_symfony_directory()
{
  checkDir=$1

  # check for .env file
  [ ! -f ${checkDir}/.env ] && { echo "$checkDir is not valid Symfony path (.env file not found)."; exit 1; }

  # check project name
  checkDirProjectName=$(cat $checkDir/.env | grep PROJECT_NAME= | cut -d '=' -f2)
  [ $checkDirProjectName != $projectName ] && { echo "PROJECT_NAME in $checkDir/.env does not match $projectName"; exit 1; }
}

while getopts o:n:p: flag
do
  case "${flag}" in
    o) oldBuildPath=${OPTARG};;
    n) newBuildPath=${OPTARG};;
    p) projectName=${OPTARG};;
  esac
done

# validate parameters
[ -z "$oldBuildPath" ] && { echo "-o is required"; exit 1; }
[ -z "$newBuildPath" ] && { echo "-n is required"; exit 1; }
[ -z "$projectName" ] && { echo "-p is required"; exit 1; }

# check if directories are valid
[ -d "${oldBuildPath}" ] &&  echo "Current build path $oldBuildPath found." || { echo "Directory $oldBuildPath not found."; }
[ -d "${newBuildPath}" ] &&  echo "New build path $newBuildPath found." || { echo "Directory $newBuildPath not found."; }

# get absolute paths
absoluteOldBuildPath=`cd $oldBuildPath; pwd`
absoluteNewBuildPath=`cd $newBuildPath; pwd`

# don't allow same path for old and new instance
[ $absoluteOldBuildPath == $absoluteNewBuildPath ] && { echo 'Same path for old and new build! Are you ok?'; exit 1; }

# validate symfony directories
verify_symfony_directory $absoluteOldBuildPath
verify_symfony_directory $absoluteNewBuildPath

rm -rf $absoluteNewBuildPath/var
<?php

namespace App\Repository;

use App\Entity\Connection;
use App\Entity\Website;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Connection|null find($id, $lockMode = null, $lockVersion = null)
 * @method Connection|null findOneBy(array $criteria, array $orderBy = null)
 * @method Connection[]    findAll()
 * @method Connection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConnectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Connection::class);
    }

    public function findLatestErrorByWebsite(Website $website): ?Connection
    {
        try {
            return $this->createQueryBuilder('c')
                ->andWhere('c.website = :website')
                ->andWhere('c.valid = 0')
                ->orderBy('c.performedAt', 'DESC')
                ->setParameters([
                    'website' => $website,
                ])
                ->setMaxResults(1)
                ->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findLatestConnectionsByWebsite(Website $website, $numberOfConnections = 15): array
    {
        $result = $this->createQueryBuilder('c')
            ->andWhere('c.website = :website')
            ->setParameter('website', $website)
            ->orderBy('c.performedAt', 'DESC')
            ->setMaxResults($numberOfConnections)
            ->getQuery()
            ->getResult();

        if (!$result) {
            return [];
        }

        return array_reverse($result);
    }
}

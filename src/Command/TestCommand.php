<?php

namespace App\Command;

use App\Repository\WebsiteRepository;
use App\Service\WebsiteTester\WebsiteTester;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(
    name: 'app:test-command',
    description: 'Add a short description for your command',
)]
class TestCommand extends Command
{
    public function __construct(
        private WebsiteTester $tester,
        private WebsiteRepository $websiteRepository,
        private MessageBusInterface $bus,
    ) {
        parent::__construct(null);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->websiteRepository->findAll() as $website) {
            $this->tester->performTest($website);
        }

        return Command::SUCCESS;
    }
}

<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static SettingEnum NOTIFICATIONS_ENABLED()
 * @method static SettingEnum NOTIFICATION_EMAIL()
 */
class SettingEnum extends Enum
{
    public const NOTIFICATIONS_ENABLED = 'notifications_enabled';
    public const NOTIFICATION_EMAIL = 'notification_email';
}

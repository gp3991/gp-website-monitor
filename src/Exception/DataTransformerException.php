<?php

namespace App\Exception;

class DataTransformerException extends \Exception
{
    public const MESSAGE_NOT_FOUND = 'Transformer not found for %s source and %s target.';

    public function __construct(string $source, string $target, $message = self::MESSAGE_NOT_FOUND)
    {
        parent::__construct(
            sprintf($message, $source, $target)
        );
    }
}

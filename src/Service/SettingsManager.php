<?php

namespace App\Service;

use App\Entity\Setting;
use App\Enum\SettingEnum;
use App\Exception\SettingNotFoundException;
use App\Repository\SettingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SettingsManager
{
    private array $settings;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private SettingRepository $settingRepository,
        ParameterBagInterface $parameterBag,
    ) {
        $this->settings = $parameterBag->get('settings');
    }

    public function notificationsEnabled(): bool
    {
        try {
            $val = $this->get(SettingEnum::NOTIFICATIONS_ENABLED())->getValue();
        } catch (SettingNotFoundException) {
            return false;
        }

        return (bool) $val;
    }

    /**
     * @throws SettingNotFoundException
     */
    public function set(SettingEnum $setting, string|bool|array $value)
    {
        $settingConfig = $this->getSettingConfigByEnum($setting);
        $entry = $this->getSettingEntryByKey($setting->getValue(), $settingConfig);

        $entry->setValue($value);
        $this->entityManager->persist($entry);
        $this->entityManager->flush();
    }

    /**
     * @throws SettingNotFoundException
     */
    public function get(SettingEnum $setting): ?Setting
    {
        $settingConfig = $this->getSettingConfigByEnum($setting);

        return $this->getSettingEntryByKey($setting->getValue(), $settingConfig);
    }

    /**
     * Find config from settings.yaml file.
     *
     * @throws SettingNotFoundException
     */
    private function getSettingConfigByEnum(SettingEnum $setting): array
    {
        if (!isset($this->settings[$setting->getValue()])) {
            throw new SettingNotFoundException($setting);
        }

        return $this->settings[$setting->getValue()];
    }

    /**
     * Find setting entry by key. Create new if not found.
     */
    private function getSettingEntryByKey(string $key, ?array $config = null): ?Setting
    {
        $settingEntry = $this->settingRepository->findOneBy(
            ['key' => $key]
        );

        if (!$settingEntry && $config) {
            $settingEntry = new Setting();
            $settingEntry->setKey($key);
            $settingEntry->setValue($config['default'] ?? '', true);
            $settingEntry->setType($config['type'] ?? Setting::DEFAULT_TYPE);
            $this->entityManager->persist($settingEntry);
            $this->entityManager->flush();
        }

        return $settingEntry;
    }
}

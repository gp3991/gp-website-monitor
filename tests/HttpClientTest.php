<?php

namespace App\Tests;

use App\Service\HttpClient\HttpClient;
use App\Service\HttpClient\HttpClientResponseInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HttpClientTest extends KernelTestCase
{
    public const ALWAYS_ONLINE_URL = 'https://google.com';
    public const ALWAYS_OFFLINE_URL = 'https://atluzr-jp2137.akwomerk';

    private HttpClient $httpClient;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->httpClient = self::getContainer()
            ->get(HttpClient::class);
    }

    public function testValidUrl(): void
    {
        $result = $this->httpClient->request(self::ALWAYS_ONLINE_URL);
        $this->assertInstanceOf(HttpClientResponseInterface::class, $result);
    }

    public function testInvalidUrl(): void
    {
        $result = $this->httpClient->request(self::ALWAYS_OFFLINE_URL);
        $this->assertIsInt($result);
        $this->assertEquals(CURLE_COULDNT_RESOLVE_HOST, $result);
    }
}

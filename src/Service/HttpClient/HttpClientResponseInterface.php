<?php

namespace App\Service\HttpClient;

interface HttpClientResponseInterface
{
    public function getStatusCode(): int;
    public function getExecutionTime(): int;
    public function getUrl(): string;
}

<?php

namespace App\Helper;

use App\Dto\Dashboard;
use App\Repository\ConnectionRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class DashboardFactory
{
    public function __construct(
        private ConnectionRepository $connectionRepository
    ) {
    }

    public function createDashboard(): Dashboard
    {
        return new Dashboard(
            $this->getUptime()
        );
    }

    private function getUptime(): float
    {
        try {
            $validConnections = $this->connectionRepository
                ->createQueryBuilder('c')
                ->select('count(c)')
                ->andWhere('c.valid = 1')
                ->getQuery()->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException) {
            $validConnections = 0;
        }

        try {
            $invalidConnections = $this->connectionRepository
                ->createQueryBuilder('c')
                ->select('count(c)')
                ->andWhere('c.valid = 0')
                ->getQuery()->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException) {
            $invalidConnections = 0;
        }

        $allConnections = $validConnections + $invalidConnections;

        if (!$allConnections) {
            return 0;
        }

        if ($invalidConnections >= $validConnections) {
            return 100;
        }

        return round($validConnections * 100 / $allConnections, 2);
    }
}

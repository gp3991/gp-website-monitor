<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static ConnectionErrorTypeEnum HTTP()
 * @method static ConnectionErrorTypeEnum INTERNAL()
 */
class ConnectionErrorTypeEnum extends Enum
{
    public const HTTP = 'http';
    public const INTERNAL = 'internal';
}

<?php

namespace App\Exception;

class SettingEnumKeyNotFoundException extends NotFoundException
{
    public const MESSAGE = 'Setting key `%s` not found in SettingEnum.';

    public function __construct(string $key)
    {
        parent::__construct(
            sprintf(self::MESSAGE, $key)
        );
    }
}

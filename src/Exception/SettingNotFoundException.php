<?php

namespace App\Exception;

use App\Enum\SettingEnum;

class SettingNotFoundException extends NotFoundException
{
    public const MESSAGE = 'Setting `%s` not found. Make sure to define it in settings.yaml file.';

    public function __construct(SettingEnum $enum)
    {
        parent::__construct(
            sprintf(self::MESSAGE, $enum->getValue())
        );
    }
}
